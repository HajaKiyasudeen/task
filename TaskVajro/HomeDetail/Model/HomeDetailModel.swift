//
//  HomeDetailModel.swift
//  TaskVajro
//
//  Created by Apple on 30/04/2021.
//

import Foundation

class HomeDetailModel: NSObject {
    
   var title: String?
   var htmlString: String?
    
    init(title:String?, htmlString: String?) {
        self.title = title
        self.htmlString = htmlString
        super.init()
    }
}
