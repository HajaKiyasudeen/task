//
//  HomeDetailViewController.swift
//  TaskVajro
//
//  Created by Apple on 30/04/2021.
//

import UIKit
import WebKit

class HomeDetailViewController: UIViewController {

    var webView: WKWebView!
    var homeDetailModel: HomeDetailModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setUpUI()
    }

    //MARK:- Set Up UI
    func setUpUI() {
        setUpNavigationView()
        setUpViews()
        setUpConstraints()
        loadRequest()
    }
    
    
    //MARK:- Set Up Views
    func setUpViews() {
               
        self.view.backgroundColor = UIColor.white
        //webView
        webView = WKWebView()
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.backgroundColor = UIColor.white
        self.view.addSubview(webView)
    }
    
    //MARK:- Set Up NavigationView
    func setUpNavigationView() {
        
        self.navigationController?.navigationBar.backgroundColor = UIColor.black
        let leftBarButton = UIBarButtonItem(image: UIImage(named: "icnBack"), style: .plain, target: self, action: #selector(backButtonAction))
        leftBarButton.tintColor = UIColor.white
        navigationItem.leftBarButtonItem = leftBarButton

        navigationItem.title = homeDetailModel?.title
    }
    
    //MARK:- Actions
    @objc func backButtonAction() {
        navigationController?.popViewController(animated: true)
    }
    
    //MARK:- loadRequest
    func loadRequest() {
        
        guard let htmlContent = homeDetailModel?.htmlString else {
            return
        }
        webView.loadHTMLString(content: htmlContent, baseURL: nil)

    }
    
    //MARK:- Set Up Constraints
    func setUpConstraints(){
        //webView
        webView.leadingAnchor.constraint(equalTo: webView.superview!.leadingAnchor).isActive = true
        webView.trailingAnchor.constraint(equalTo: webView.superview!.trailingAnchor).isActive = true
        webView.topAnchor.constraint(equalTo: webView.superview!.layoutMarginsGuide.topAnchor,constant: 0).isActive = true
        webView.bottomAnchor.constraint(equalTo: webView.superview!.bottomAnchor).isActive = true

    }
}
