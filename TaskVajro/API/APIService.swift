//
//  APIService.swift
//  TaskVajro
//
//  Created by Apple on 29/04/2021.
//

import Foundation
import Alamofire

//enum APIResult<T, APIError> {
//    case success(T)
//    case error(APIError)
//}

class APIService: SessionManager {
    
    static let shared = APIService()
    
    private init() {
        super.init()
        
        session.configuration.httpAdditionalHeaders?.updateValue(["application/json", "text/html", "image/png", "image/jpeg"], forKey: "Content-Type")
    }
    
    fileprivate func validateAF<T: BaseModel>(response: DataResponse<T>) -> (successModel: T?, error: Error?) {
        
        switch response.result {
        case .success(let parsedModel):
            return (parsedModel, nil)
        case .failure(let error):
            print(error)
            return (nil, error)
        }
    }

}

extension APIService {
    
    func homeListAPI<T: BaseModel>(success: @escaping (T) -> Void,
                                   failuer: @escaping (Error?) -> Void) {
        guard isNetwork(failuer: failuer) else { return }
        
        request(homeListURL()).responseDecodable { [weak self] (response: DataResponse<T>) in
            
            guard let parsedResponse = self?.validateAF(response: response).successModel else {
                failuer(response.error ?? APIError.parsingFailed)
                return
            }
            
            success(parsedResponse)
        }
        
    }
}

