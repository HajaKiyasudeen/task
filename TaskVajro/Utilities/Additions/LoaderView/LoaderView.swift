//
//  LoaderView.swift
//  TaskVajro
//
//  Created by Apple on 30/04/2021.
//

import UIKit

class LoaderView: NSObject {
    
    static var loadIconDictionary:[UIView: ActivityIndicatorLoaderView] = [:]
    
    static func addLoadIcon(_ view: UIView) -> Void {
        view.isUserInteractionEnabled = false
        
        if let activityIndicatorView = loadIconDictionary[view] {
            view.bringSubviewToFront(activityIndicatorView)
            return
        } else {
            let activityIndicatorView = ActivityIndicatorLoaderView()
            view.addSubview(activityIndicatorView)
            
            var constraintsArray: [NSLayoutConstraint] = []
            
            constraintsArray.append(activityIndicatorView.leadingAnchor.constraint(equalTo: view.leadingAnchor))
            constraintsArray.append(activityIndicatorView.trailingAnchor.constraint(equalTo: view.trailingAnchor))
            constraintsArray.append(activityIndicatorView.topAnchor.constraint(equalTo: view.topAnchor))
            constraintsArray.append(activityIndicatorView.bottomAnchor.constraint(equalTo: view.bottomAnchor))
            
            NSLayoutConstraint.activate(constraintsArray)
            
            loadIconDictionary[view] = activityIndicatorView
        }
    }
    
    static func removeLoadIcon(_ view: UIView) -> Void {
        view.isUserInteractionEnabled = true
        
        if let activityIndicatorView = loadIconDictionary[view] {
            activityIndicatorView.animate(type: .fadeOut(0.0), completion: { isSuccess in
                activityIndicatorView.stopIndicatorView()
                loadIconDictionary[view] = nil
            })
        }
    }
}

class ActivityIndicatorLoaderView: UIView {
    
    private lazy var activityIndicatorView = UIActivityIndicatorView.activityFor(self)
    
    required init() {
        super.init(frame: .zero)
        
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        generalUISetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: Initial UI Setup
    
    private func generalUISetup() {
        alpha = 0
        
        activityIndicatorView.tintColor = .white
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [weak self] in
            self?.animate(type: .fadeIn(1.0))
        }
    }
    
    func stopIndicatorView() {
        activityIndicatorView.stopAnimating()
        removeFromSuperview()
    }
}

