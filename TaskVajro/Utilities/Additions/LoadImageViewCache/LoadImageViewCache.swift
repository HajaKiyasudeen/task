//
//  LoadImageViewCache.swift
//  Gaawk
//
//  Created by Apple on 30/04/2021.
//

import Foundation
import Alamofire

let imageCache = NSCache<AnyObject, AnyObject>()

@objc class LoadImageViewCache: UIImageView {

    var imageURL: URL?

    let activityIndicator = UIActivityIndicatorView()

    @objc func loadImageWithUrl(_ url: URL, resizeImageSize: CGSize = .zero, completionHandler:@escaping() -> Void) {

        // setup activityIndicator...
        activityIndicator.color = .darkGray

        addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true

        imageURL = url

//        image = nil
        activityIndicator.startAnimating()

        // retrieves image if already available in cache
        if let imageFromCache = imageCache.object(forKey: url as AnyObject) as? UIImage {

            self.image = imageFromCache.resizeIfNeed(resizeImageSize)
            activityIndicator.stopAnimating()
            completionHandler()
            return
        }

        
        // image does not available in cache.. so retrieving it from url...
        URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in

            if error != nil {
                print(error as Any)
                DispatchQueue.main.async(execute: {
                    self.activityIndicator.stopAnimating()
                })
                return
            }

            DispatchQueue.main.async(execute: {

                if let unwrappedData = data, let imageToCache = UIImage(data: unwrappedData) {

                    if self.imageURL == url {
                        self.image = imageToCache.resizeIfNeed(resizeImageSize)
                        completionHandler()
                    }

                    imageCache.setObject(imageToCache, forKey: url as AnyObject)
                    
                }
                self.activityIndicator.stopAnimating()
            })
        }).resume()
    }
}

extension UIImage {
    
    fileprivate func resizeIfNeed(_ withSize: CGSize) -> UIImage? {
        return withSize == CGSize.zero ? self:resizeImage(withSize)
    }
    
    private func resizeImage(_ newSize: CGSize) -> UIImage? {
        return isSameSize(newSize) ? self:scaleImage(newSize)!
    }
    
    private func isSameSize(_ newSize: CGSize) -> Bool {
        return size == newSize
    }
    
    private func scaleImage(_ newSize: CGSize) -> UIImage? {
        return _scaleImage(getScaledRect(newSize))
    }
    
    private func getScaledRect(_ newSize: CGSize) -> CGRect {
        let ratio   = max(newSize.width / size.width, newSize.height / size.height)
        let width   = size.width * ratio
        let height  = size.height * ratio
        return CGRect(x: 0, y: 0, width: width, height: height)
    }
    
    private func _scaleImage(_ scaledRect: CGRect) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(scaledRect.size, false, 0.0);
        draw(in: scaledRect)
        let image = UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
        UIGraphicsEndImageContext()
        return image
    }
    
}
