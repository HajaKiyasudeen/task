//
//  UIColor+Additions.swift
//  TaskVajro
//
//  Created by Apple on 29/04/2021.
//

import UIKit

extension UIColor {

    class var paleGray : UIColor {
        return UIColor(red: 238.0 / 255.0, green: 238.0 / 255.0, blue: 238.0 / 255.0, alpha: 1.0)
    }
}
