//
//  Constant.swift
//  TaskVajro
//
//  Created by Apple on 30/04/2021.
//

import UIKit

class Constant {

    public static let ScreenWidth : CGFloat = UIScreen.main.bounds.width
    
    public static let ScreenHeight : CGFloat = UIScreen.main.bounds.height

}
