//
//  StringConstant.swift
//  TaskVajro
//
//  Created by Apple on 29/04/2021.
//

import Foundation

enum StringConstant {
    
    enum Common {
        static let appName = "TaskVajro"
        static let okey = "Okay"
        static let cancel = "Cancel"
        static let noNetwork = "No network available"
        static let timeout = "Request timeout! Try again."
        static let somethingWentWrong = "Something went wrong, Please try again!"
        static let imageViewADA = "image"
    }
}
