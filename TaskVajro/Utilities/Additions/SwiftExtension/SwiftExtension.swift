//
//  SwiftExtension.swift
//  TaskVajro
//
//  Created by Apple on 29/04/2021.
//

import UIKit
import WebKit

extension UIViewController {
    
    func showAlert(message: String) {
        
        let alert = UIAlertController(title: "", message: message as String, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
        
    }
}

extension UITableViewCell
{
    static var reuseIdentifier: String {
        let className = String(describing: self)
        return className
    }
}


extension Error {
    
    func isTimeOutError(_ fromViewController: UIViewController? = nil) -> Bool {
        func checkAndShow(alert string: String) -> Void {
            if let vc = fromViewController {
                vc.showAlert(message: string)
            }
        }
        
        var returnValue = true
        
        switch _code {
        case NSURLErrorDNSLookupFailed, NSURLErrorNotConnectedToInternet, NSURLErrorNetworkConnectionLost, NSURLErrorCannotConnectToHost, NSURLErrorCannotFindHost:
            checkAndShow(alert: StringConstant.Common.noNetwork)
        case NSURLErrorTimedOut:
            checkAndShow(alert: StringConstant.Common.timeout)
        default:
            if let sgerror = self as? APIService.APIError {
                switch sgerror {
                case .parsingFailed, .somethingWrong:
                    checkAndShow(alert: StringConstant.Common.somethingWentWrong)
                case .noNetwork:
                    checkAndShow(alert: StringConstant.Common.noNetwork)
                }
            } else {
                returnValue = false
            }
        }
        
        return returnValue
    }
    
}

extension Optional where Wrapped == Error {
    func  isTimeOutError(_ fromViewController: UIViewController? = nil) -> Bool {
        if let error = self {
            return error.isTimeOutError(fromViewController)
        }
        //Remove load icon
        return false
    }
}

extension UIView {
    public typealias kCompletionHandler = (Bool) -> Void

    enum DDAnimationType {
        case fadeIn(CGFloat) // alpha to Value
        case fadeOut(CGFloat) // alpha to Value : View won't remove or hide
    }

    func animate(type: DDAnimationType,
                 with duration: TimeInterval = 0.3,
                 delay: TimeInterval = 0.0,
                 options: UIView.AnimationOptions = [],
                 animations: (() -> Void)? = nil,
                 completion: kCompletionHandler? = nil) {
        func startAnimate() {
            UIView.animate(withDuration: duration,
                           delay: 0,
                           options: options,
                           animations: { [weak self] in
                            switch type {
                            case .fadeIn(let alphaValue),
                                 .fadeOut(let alphaValue):
                                self?.alpha = alphaValue
                            }
                            animations?()
                           })
        }
        func completionAtEnd() {
            // UIView animate doesn't work as expected with completion handuler so creating static dispatch queue with animation duration
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + duration, execute: { () -> Void in
                completion?(true)
            })
        }

        // UIView animate doesn't work as expected with delay so creating static dispatch queue delay only if delay greater than 0
        if delay > 0 {
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delay, execute: { () -> Void in
                startAnimate()
                completionAtEnd()
            })
        } else {
            startAnimate()
            completionAtEnd()
        }
    }
}

extension UIActivityIndicatorView {
    
    class func activityFor(_ view: UIView,
                           height: CGFloat = 35.0,
                           width: CGFloat = 35.0) -> UIActivityIndicatorView {
        let activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView.init(style: .gray)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.hidesWhenStopped = true
        activityIndicator.color = .darkGray
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        
        var constraintsArray: [NSLayoutConstraint] = []
        
        constraintsArray.append(activityIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor))
        constraintsArray.append(activityIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor))
        constraintsArray.append(activityIndicator.widthAnchor.constraint(equalToConstant: width))
        constraintsArray.append(activityIndicator.heightAnchor.constraint(equalToConstant: height))
        
        NSLayoutConstraint.activate(constraintsArray)
        
        return activityIndicator
    }

}

extension WKWebView {

    func loadHTMLString(content:String,baseURL:URL?){
        let headerString = "<header><meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'></header>"
        loadHTMLString(headerString + content, baseURL: baseURL)
    }
}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return nil
        }
    }
}
