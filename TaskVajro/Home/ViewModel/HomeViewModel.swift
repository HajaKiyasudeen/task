//
//  HomeViewModel.swift
//  TaskVajro
//
//  Created by Apple on 30/04/2021.
//

import Foundation
import Alamofire

// MARK: - HomeViewModel
class HomeViewModel: NSObject {
    
    typealias kDefaultHandler = (() -> Void)
    
    fileprivate(set) var articleData : ArticleModel? {
        didSet {
            guard self.articleData != nil else { return }
            self.commonError = nil
            self.bindingHomeViewModelToController()
        }
    }
    fileprivate(set) var commonError: Error? {
        didSet {
            guard self.commonError != nil else { return }
            self.articleData = nil
            self.bindingHomeViewModelToController()
        }
    }
    
    let bindingHomeViewModelToController : kDefaultHandler
    
    init(handuler: @escaping kDefaultHandler) {
        bindingHomeViewModelToController = handuler
        
        super.init()
        
        initiateAPICall()
    }
    
    func reloadHomeListData() {
        initiateAPICall()
    }
    
    
    private func initiateAPICall() {
        APIService.shared.homeListAPI { [weak self] (articleModel: ArticleModel) in
            print("success")
            DispatchQueue.main.async {
                self?.articleData = articleModel
            }
        } failuer: { [weak self] (error) in
            print("failure")
            DispatchQueue.main.async {
                self?.commonError = error
            }
        }
    }
}
