//
//  Article.swift
//  TaskVajro
//
//  Created by Apple on 30/04/2021.
//

import Foundation

// MARK: - Welcome
struct ArticleModel: BaseModel {
    let articles: [Article]?
    var status: String?
    
    func with(
        articles: [Article]?? = nil,
        status: String?? = nil
    ) -> ArticleModel {
        return ArticleModel(
            articles: articles ?? self.articles,
            status: status ?? self.status
        )
    }
}
// MARK: - Article
struct Article: BaseModel {
    var id: Int?
    var title: String?
    var createdAt: Date?
    var bodyHTML: String?
    var blogID: Int?
    var author: String?
    var userID: Int?
    var publishedAt, updatedAt: Date?
    var summaryHTML: String?
    var templateSuffix: JSONNull?
    var handle, tags, adminGraphqlAPIID: String?
    var image: Image?

    enum CodingKeys: String, CodingKey {
        case id, title
        case createdAt = "created_at"
        case bodyHTML = "body_html"
        case blogID = "blog_id"
        case author
        case userID = "user_id"
        case publishedAt = "published_at"
        case updatedAt = "updated_at"
        case summaryHTML = "summary_html"
        case templateSuffix = "template_suffix"
        case handle, tags
        case adminGraphqlAPIID = "admin_graphql_api_id"
        case image
    }
}

extension Article {
    
    func with(
          id: Int?? = nil,
          title: String?? = nil,
          createdAt: Date?? = nil,
          bodyHTML: String?? = nil,
          blogID: Int?? = nil,
          author: String?? = nil,
          userID: Int?? = nil,
          publishedAt: Date?? = nil,
          updatedAt: Date?? = nil,
          summaryHTML: String?? = nil,
          templateSuffix: JSONNull?? = nil,
          handle: String?? = nil,
          tags: String?? = nil,
          adminGraphqlAPIID: String?? = nil,
          image: Image?? = nil
      ) -> Article {
          return Article(
              id: id ?? self.id,
              title: title ?? self.title,
              createdAt: createdAt ?? self.createdAt,
              bodyHTML: bodyHTML ?? self.bodyHTML,
              blogID: blogID ?? self.blogID,
              author: author ?? self.author,
              userID: userID ?? self.userID,
              publishedAt: publishedAt ?? self.publishedAt,
              updatedAt: updatedAt ?? self.updatedAt,
              summaryHTML: summaryHTML ?? self.summaryHTML,
              templateSuffix: templateSuffix ?? self.templateSuffix,
              handle: handle ?? self.handle,
              tags: tags ?? self.tags,
              adminGraphqlAPIID: adminGraphqlAPIID ?? self.adminGraphqlAPIID,
              image: image ?? self.image
          )
      }
    
    func transfromToDetailModel() -> HomeDetailModel {
        return HomeDetailModel(title: title, htmlString: bodyHTML)
    }
}

// MARK: - Image
struct Image: BaseModel {
    var createdAt: Date?
    var alt: String?
    var width, height: Int?
    var src: String?

    enum CodingKeys: String, CodingKey {
        case createdAt = "created_at"
        case alt, width, height, src
    }
    
    
    func with(
        createdAt: Date?? = nil,
        alt: String?? = nil,
        width: Int?? = nil,
        height: Int?? = nil,
        src: String?? = nil
    ) -> Image {
        return Image(
            createdAt: createdAt ?? self.createdAt,
            alt: alt ?? self.alt,
            width: width ?? self.width,
            height: height ?? self.height,
            src: src ?? self.src
        )
    }
}

// MARK: - Encode/decode helpers

class JSONNull: Codable, Hashable {

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }

    public var hashValue: Int {
        return 0
    }

    public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}
