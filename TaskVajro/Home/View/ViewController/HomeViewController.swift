//
//  HomeViewController.swift
//  TaskVajro
//
//  Created by Apple on 29/04/2021.
//

import UIKit

class HomeViewController: UIViewController {

    var tableView : UITableView!
    var refreshControl: UIRefreshControl!

    fileprivate lazy var viewModel = HomeViewModel(handuler: apiHandler)

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setUpUI()
    }

    //MARK:- Set Up UI
    func setUpUI() {
        let _ = viewModel // Invoke viewmodel
        setUpNavigationView()
        setUpViews()
        setUpConstraints()
        LoaderView.addLoadIcon(self.view)

    }
    
    //MARK:- Set Up NavigationView
    func setUpNavigationView() {
        
        navigationController?.navigationBar.backgroundColor = UIColor.black
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.view.backgroundColor = UIColor.clear

        let navigationTitleAttribute = [NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 15)]
        navigationController?.navigationBar.titleTextAttributes = navigationTitleAttribute
        navigationItem.title = "Blogs"

    }

    //MARK:- Set Up Views
    func setUpViews() {
        //tableView
        tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(HomeTableViewCell.self, forCellReuseIdentifier: HomeTableViewCell.reuseIdentifier)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.backgroundColor = .paleGray
        self.view.addSubview(tableView)
        
        //refreshControl
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        refreshControl.tintColor = UIColor.black
        tableView.addSubview(refreshControl)

    }
    
    func reloadTableViewCell(article: Article) {
        
        guard let haveValue = viewModel.articleData?.articles else { return }
        
        guard let haveIndex = haveValue.firstIndex(where: {$0.image?.src == article.image?.src})  else {
            return
        }
        self.tableView.reloadRows(at: [IndexPath(row: haveIndex,section: 0)], with: .automatic)
    }
    
    //MARK:- Actions
    @objc func backButtonAction() {
        navigationController?.popViewController(animated: true)
    }
    
    @objc func refresh(_ sender: Any) {
        viewModel.reloadHomeListData()
    }
    
    //MARK:- Set Up Constraints
    func setUpConstraints(){
        //tableView
        tableView.leadingAnchor.constraint(equalTo: tableView.superview!.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: tableView.superview!.trailingAnchor).isActive = true
        tableView.topAnchor.constraint(equalTo: tableView.superview!.layoutMarginsGuide.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: tableView.superview!.bottomAnchor).isActive = true
    }
    
    fileprivate func apiHandler() {
        refreshControl.endRefreshing()

        guard !viewModel.commonError.isTimeOutError(self) else {
            showAlert(message: viewModel.commonError?.localizedDescription ?? StringConstant.Common.somethingWentWrong)
            return
        }
        LoaderView.removeLoadIcon(self.view)
        tableView.reloadData()
    }
}

//MARK:- UITableView Delegate & DataSource
extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let resultcount = viewModel.articleData?.articles else { return 0}
        return resultcount.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: HomeTableViewCell.reuseIdentifier) as? HomeTableViewCell {
            cell.selectionStyle = .none
            
            cell.imageDownloaded = reloadTableViewCell(article:)
            guard let haveValue = viewModel.articleData?.articles?[indexPath.row] else { return  cell }
            cell.getArticleModel = haveValue
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let haveValue = viewModel.articleData?.articles?[indexPath.row] else { return }
        let homeDetailViewController = HomeDetailViewController()
        homeDetailViewController.homeDetailModel = haveValue.transfromToDetailModel()
        self.navigationController?.pushViewController(homeDetailViewController, animated: true)
    }
}
