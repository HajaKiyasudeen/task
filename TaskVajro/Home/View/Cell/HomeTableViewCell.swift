//
//  HomeTableViewCell.swift
//  TaskVajro
//
//  Created by Apple on 29/04/2021.
//

import UIKit

class HomeTableViewCell: UITableViewCell {

    var contentBGView: UIView!
    var contentImageView: LoadImageViewCache!
    var titleLabel: UILabel!
    var descTextView: UITextView!
    var imageDownloaded: ((Article) -> Void)? = nil
    
    var getArticleModel : Article? {
        didSet {
            titleLabel.text = getArticleModel?.title
            
            //description
            if getArticleModel?.summaryHTML != nil {
                let isHaveHtml = isHtml((getArticleModel?.summaryHTML!)!)
                if isHaveHtml {
                    descTextView.attributedText = getArticleModel?.summaryHTML?.htmlToAttributedString
                }
                else {
                    descTextView.text = getArticleModel?.summaryHTML
                }
            }
            
            // rendering image
            contentImageView.image = UIImage(named: "imagePlaceholder")
            guard let image = getArticleModel?.image else { return }
            guard let imageSrc = image.src else { return }
            if let imgUrl = URL(string: imageSrc) {
                contentImageView.loadImageWithUrl(imgUrl, resizeImageSize: CGSize(width: Constant.ScreenWidth - 20, height: 250),completionHandler: {
                    
                    guard let checkImage = self.imageDownloaded else {
                        return
                    }
                    
                    guard let getArticle = self.getArticleModel else {
                        return
                    }
                    checkImage(getArticle)
                })
            }
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setUpUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUpUI() {
        setUpViews()
        setUpConstraints()
    }
    
    //MARK:- Set Up Views
    func setUpViews() {
        self.contentView.backgroundColor = .paleGray

        //contentBGView
        contentBGView = UIView()
        contentBGView.translatesAutoresizingMaskIntoConstraints = false
        contentBGView.backgroundColor = UIColor.white
        contentBGView.layer.cornerRadius = 10
        contentBGView.layer.masksToBounds = true
        contentView.addSubview(contentBGView)

        //contentImageView
        contentImageView = LoadImageViewCache()
        contentImageView.translatesAutoresizingMaskIntoConstraints = false
        contentImageView.backgroundColor = UIColor.lightGray
        contentImageView.contentMode = .scaleAspectFit
        contentBGView.addSubview(contentImageView)
        
        //titleLabel
        titleLabel = UILabel()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.textColor = UIColor.black
        titleLabel.font = UIFont.boldSystemFont(ofSize: 17)
        titleLabel.text = "faddsff"
        contentBGView.addSubview(titleLabel)

        //descTextView
        descTextView = UITextView()
        descTextView.translatesAutoresizingMaskIntoConstraints = false
        descTextView.isScrollEnabled = false
        descTextView.isUserInteractionEnabled = false
        descTextView.textContainer.maximumNumberOfLines = 2
        descTextView.font = UIFont.systemFont(ofSize: 15)
        descTextView.backgroundColor = UIColor.clear
        contentBGView.addSubview(descTextView)
    }
    
    //MARK:- Set Up Constraints
    func setUpConstraints() {
        
        //contentBGView
        self.contentBGView.leadingAnchor.constraint(equalTo: self.contentBGView.superview!.leadingAnchor, constant: 10).isActive = true
        self.contentBGView.trailingAnchor.constraint(equalTo: self.contentBGView.superview!.trailingAnchor, constant: -10).isActive = true
        self.contentBGView.topAnchor.constraint(equalTo: self.contentBGView.superview!.topAnchor, constant: 12).isActive = true
        self.contentBGView.bottomAnchor.constraint(equalTo: self.contentBGView.superview!.bottomAnchor, constant: 0).isActive = true
        
        //leftImageView
        self.contentImageView.leadingAnchor.constraint(equalTo: self.contentImageView.superview!.leadingAnchor, constant: 0).isActive = true
        self.contentImageView.trailingAnchor.constraint(equalTo: self.contentImageView.superview!.trailingAnchor, constant: 0).isActive = true
        self.contentImageView.topAnchor.constraint(equalTo: self.contentImageView.superview!.topAnchor, constant: 0).isActive = true
        self.contentImageView.heightAnchor.constraint(equalToConstant: 350).isActive = true

        //titleLabel
        self.titleLabel.leadingAnchor.constraint(equalTo: self.titleLabel.superview!.leadingAnchor, constant: 10).isActive = true
        self.titleLabel.trailingAnchor.constraint(equalTo: self.titleLabel.superview!.trailingAnchor, constant: -10).isActive = true
        self.titleLabel.topAnchor.constraint(equalTo: self.contentImageView.bottomAnchor, constant: 5).isActive = true

        //descTextView
        self.descTextView.leadingAnchor.constraint(equalTo: self.titleLabel.leadingAnchor).isActive = true
        self.descTextView.trailingAnchor.constraint(equalTo: self.titleLabel.trailingAnchor).isActive = true
        self.descTextView.topAnchor.constraint(equalTo: self.titleLabel.bottomAnchor, constant: 10).isActive = true
        self.descTextView.bottomAnchor.constraint(equalTo: self.descTextView.superview!.bottomAnchor, constant: -10).isActive = true

    }
    //MARK:- Check is Html
    private func isHtml(_ value:String) -> Bool {
        let validateTest = NSPredicate(format:"SELF MATCHES %@", "<([a-z][a-z0-9]*)\\b[^>]*>(.*?)</\\1>")
        return validateTest.evaluate(with: value)
    }
}
